from pathlib import Path

from openlog.datamodel.assay.generic_assay import (
    AssayColumn,
    AssayDatabaseDefinition,
    AssayDataExtent,
    AssayDefinition,
    AssayDomainType,
    AssaySeriesType,
)


def create_file(
    tmp_path: Path, filename: str, columns: [str], values: [[str]], delimiter: str
) -> Path:
    csv_file = tmp_path / filename
    with csv_file.open("a") as fp:
        fp.write(delimiter.join(columns) + "\n")
        for row in values:
            fp.write(delimiter.join(row) + "\n")
    return csv_file


def create_assay_discrete_definition_object(
    assay_name: str,
    schema: str,
    assay_domain: AssayDomainType,
    series_type: AssaySeriesType,
) -> (AssayDefinition, AssayDatabaseDefinition):
    """
    Create assay definition object AssayDefinition and AssayDatabaseDefinition for discrete data extent

    Args:
        assay_name: Assay name
        schema: schema name
        assay_domain: Assay domain
        series_type: Series type

    Returns: (AssayDefinition, AssayDatabaseDefinition)

    """
    definition = AssayDefinition(
        assay_name,
        assay_domain,
        AssayDataExtent.DISCRETE,
        {"y": AssayColumn("y", series_type)},
    )
    table = AssayDatabaseDefinition(
        table_name=assay_name,
        hole_id_col="hole_id",
        dataset_col="dataset",
        x_col="x",
        y_col={"y": "y"},
        schema=schema,
    )
    return definition, table


def create_assay_extended_definition_object(
    assay_name: str,
    schema: str,
    assay_domain: AssayDomainType,
    series_type: AssaySeriesType,
) -> (AssayDefinition, AssayDatabaseDefinition):
    """
    Create assay definition object AssayDefinition and AssayDatabaseDefinition for extended data extent

    Args:
        assay_name: Assay name
        schema: schema name
        assay_domain: Assay domain
        series_type: Series type

    Returns: (AssayDefinition, AssayDatabaseDefinition)

    """
    definition = AssayDefinition(
        assay_name,
        assay_domain,
        AssayDataExtent.EXTENDED,
        {"y": AssayColumn("y", series_type)},
    )

    table = AssayDatabaseDefinition(
        table_name=assay_name,
        hole_id_col="hole_id",
        dataset_col="dataset",
        x_col="x",
        x_end_col="x_end",
        y_col={"y": "y"},
        schema=schema,
    )
    return definition, table
