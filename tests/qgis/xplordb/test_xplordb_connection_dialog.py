import pytest
from PyQt5.QtWidgets import QMessageBox

from openlog.datamodel.connection.openlog_connection import Connection
from openlog.gui.connection.xplordb_connection_dialog import XplordbConnectionDialog


@pytest.fixture()
def connection_dialog(qtbot, connection: Connection):
    """
    Fixture to create a XplordbConnectionDialog with connection parameters

    Args:
        qtbot: qtbot fixture
        connection: connection parameters
    """
    widget = XplordbConnectionDialog()
    widget.set_connection_model(connection)
    qtbot.addWidget(widget)
    yield widget


def test_connection_clean_db(connection_dialog, clean_db, mocker):
    """
    Test XplordbConnectionDialog with clean database (no xplordb schema)

    Args:
        connection_dialog: : fixture to create a XplordbConnectionDialog
        clean_db: fixture to create a clean database
        mocker: pytest-mock to simulate QMessageBox return value
    """
    # mock no xplordb schema without database creation
    msg_box_cancel_mock = mocker.patch.object(QMessageBox, 'warning', return_value=QMessageBox.Cancel)
    assert not connection_dialog.is_valid()
    msg_box_cancel_mock.assert_called_once()


def test_connection_clean_db_creation(connection_dialog, clean_db, mocker):
    """
    Test XplordbConnectionDialog with clean database (no xplordb schema) and schema creation

    Args:
        connection_dialog: : fixture to create a XplordbConnectionDialog
        clean_db: fixture to create a clean database
        mocker: pytest-mock to simulate QMessageBox return value
    """

    # mock no xplordb schema with database creation
    msg_box_ok_mock = mocker.patch.object(QMessageBox, 'warning', return_value=QMessageBox.Ok)
    assert connection_dialog.is_valid()
    msg_box_ok_mock.assert_called_once()


def test_connection_lite_db(connection_dialog, litedb_no_data):
    """
    Test XplordbConnectionDialog with litedb schema

    Args:
        connection_dialog: : fixture to create a XplordbConnectionDialog
        litedb_no_data: fixture to create a lite xplordb database
    """
    assert connection_dialog.is_valid()
