import pytest

from openlog.gui.connection.geotic_connection_dialog import GeoticConnectionDialog


@pytest.fixture
def plugin_geotic_connection(openlog_plugin, connection, mocker):
    # mock connection model definition
    mocker.patch.object(GeoticConnectionDialog, "exec", return_value=True)
    mocker.patch.object(
        GeoticConnectionDialog, "get_connection_model", return_value=connection
    )
    openlog_plugin._geotic_connect()
    yield openlog_plugin


def test_geotic_connection(plugin_geotic_connection):
    # Check that surveying action is not enabled
    assert not plugin_geotic_connection.selected_colar_desurv_action.isEnabled()

    # Check that feature are available in collar layer
    assert (
        plugin_geotic_connection.openlog_connection.get_layers_iface()
        .get_collar_layer()
        .featureCount()
        == 44
    )

    # Check that no trace are available
    assert (
        plugin_geotic_connection.openlog_connection.get_layers_iface()
        .get_collar_trace_layer()
        .featureCount()
        == 0
    )


def test_geotic_survey_select(plugin_geotic_connection):
    plugin_geotic_connection.openlog_connection.get_layers_iface().get_collar_layer().selectAll()

    # Check that desurveying action is enabled
    assert plugin_geotic_connection.selected_colar_desurv_action.isEnabled()

    plugin_geotic_connection._selected_collar_desurveying()

    # Check that feature are available in collar trace layer
    # Need to clone layer otherwise feature count is invalid. Already try a reload but no new feature available...
    layer = (
        plugin_geotic_connection.openlog_connection.get_layers_iface()
        .get_collar_trace_layer()
        .clone()
    )
    assert layer.featureCount() == 44
