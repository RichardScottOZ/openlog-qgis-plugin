from dataclasses import dataclass
from enum import Enum


class UncertaintyType(Enum):
    # Uncertainty is undefined
    UNDEFINED = "undefined"
    # Uncertainty is defined by one value : upper_whisker_column for wide interval
    ONE_COLUMN = "one_column"
    # Uncertainty is defined by two values :
    # upper_whisker_column for max wide interval
    # lower_whisker_column for min wide interval
    TWO_COLUMN = "two_column"
    # Uncertainty is defined by four values :
    # upper_whisker_column for max wide interval
    # lower_whisker_column for min wide interval
    # upper_box_column for max narrow interval
    # lower_box_column for min narrow interval
    FOUR_COLUMN = "four_column"


@dataclass
class AssayColumnUncertainty:
    upper_whisker_column: str = ""
    lower_whisker_column: str = ""
    upper_box_column: str = ""
    lower_box_column: str = ""

    def get_uncertainty_columns(self) -> [str]:
        """
        Get uncertainty columns

        Returns: [str] uncertainty columns

        """
        result = []
        uncertainty_type = self.get_uncertainty_type()
        if uncertainty_type == UncertaintyType.ONE_COLUMN:
            result.append(self.upper_whisker_column)
        elif uncertainty_type == UncertaintyType.TWO_COLUMN:
            result.append(self.upper_whisker_column)
            result.append(self.lower_whisker_column)
        elif uncertainty_type == UncertaintyType.FOUR_COLUMN:
            result.append(self.upper_whisker_column)
            result.append(self.lower_whisker_column)
            result.append(self.upper_box_column)
            result.append(self.lower_box_column)
        return result

    def get_uncertainty_type(self) -> UncertaintyType:
        result = UncertaintyType.UNDEFINED
        if self.upper_whisker_column:
            result = UncertaintyType.ONE_COLUMN
        if self.upper_whisker_column and self.lower_whisker_column:
            result = UncertaintyType.TWO_COLUMN
            if self.upper_box_column and self.lower_box_column:
                result = UncertaintyType.FOUR_COLUMN
        return result
