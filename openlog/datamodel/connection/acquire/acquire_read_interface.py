from sqlalchemy.orm import Session, declarative_base
from xplordb.datamodel.survey import Survey

from openlog.datamodel.connection.sqlalchemy.sqlalchemy_read_interface import (
    SqlAlchemyReadInterface,
)

Base = declarative_base()


class AcquireReadInterface(SqlAlchemyReadInterface):
    def __init__(
        self,
        session: Session,
        person_base: Base,
        dataset_base: Base,
        collar_base: Base,
        survey_base: Base,
        lith_base: Base = None,
    ):
        """
        Implement ReadInterface with an Acquire session and description of table

        Args:
            session: sqlalchemy session created from engine
            person_base: sqlalchemy base describing person object
            dataset_base: sqlalchemy base describing dataset object
            collar_base: sqlalchemy base describing collar object
            survey_base: sqlalchemy base describing survey object
            lith_base: sqlalchemy base describing lith object
        """
        super().__init__(
            session=session,
            person_base=person_base,
            dataset_base=dataset_base,
            collar_base=collar_base,
            survey_base=survey_base,
            lith_base=lith_base,
        )

    def get_surveys_from_collars(self, collars_id: [str]) -> [Survey]:
        """
        Return surveys from collar id list

        """
        # Only use survey with valid azimuth and dip
        surveys = (
            self.session.query(self.survey_base)
            .filter(
                self.survey_base.hole_id.in_(collars_id),
                self.survey_base.azimuth.is_not(None),
                self.survey_base.dip.is_not(None),
            )
            .all()
        )
        return surveys
