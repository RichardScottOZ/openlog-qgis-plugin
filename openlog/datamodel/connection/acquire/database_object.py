from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy import ForeignKey, Float
from sqlalchemy import Column, String

from xplordb.datamodel.collar import Collar
from xplordb.datamodel.dataset import Dataset
from xplordb.datamodel.survey import Survey

Base = declarative_base()

ACQUIRE_PERSON_CODE: str = 'acquire'
ACQUIRE_DATASET: str = 'acquire_dataset'


class AcquireDataset(Dataset, Base):
    """
    sqlalchemy Base describing AcquireDataset
    """
    __tablename__ = 'PROJECT'
    name = Column("PROJECTCODE", String, primary_key=True)
    full_name = Column("PROJECTNAME", String)
    loaded_by = ACQUIRE_PERSON_CODE


class CollarCoordinates(Base):
    """
    sqlalchemy Base describing collar_base coordinates in Acquire database

    Missing in current version :
    - transform x / y / z value to point or get corresponding SRID

    """
    __tablename__ = "HOLECOORD"
    data_set = Column("PROJECTCODE", String, ForeignKey('PROJECT.PROJECTCODE'), primary_key=True)
    hole_id = Column("HOLEID", String, ForeignKey('HOLELOCATION.HOLEID'), primary_key=True)
    coordinate_set = Column("COORDINATESET", String, primary_key=True)
    x = Column('X', String)
    y = Column('Y', String)
    z = Column('Z', String)


class AcquireCollar(Collar, Base):
    """
    sqlalchemy Base describing collar_base in Acquire database

    Missing in current version :
    - transform x / y / z value to point or get corresponding SRID

    """
    __tablename__ = "HOLELOCATION"
    hole_id = Column('HOLEID', String, primary_key=True)
    data_set = Column('PROJECTCODE', String, primary_key=True)
    loaded_by = ACQUIRE_PERSON_CODE
    srid = 32218

    eoh = None
    survey_date = None

    # cascade option not fully tested, used for some test of collar_base creation not yet implemented
    coord = relationship("CollarCoordinates", cascade="all, delete, delete-orphan")

    @property
    def x(self) -> float:
        if self.coord:
            return self.coord[0].x
        else:
            return 0.0

    @x.setter
    def x(self, val: float):
        self._add_coordinates_if_needed()
        self.coord[0].x = val

    @property
    def y(self) -> float:
        if self.coord:
            return self.coord[0].y
        else:
            return 0.0

    @y.setter
    def y(self, val: float):
        self._add_coordinates_if_needed()
        self.coord[0].y = val

    @property
    def z(self) -> float:
        if self.coord:
            return self.coord[0].z
        else:
            return 0.0

    @z.setter
    def z(self, val: float):
        self._add_coordinates_if_needed()
        self.coord[0].z = val

    def _add_coordinates_if_needed(self):
        """
        Add CollarCoordinates if not available

        """
        if not self.coord:
            coord = CollarCoordinates()
            coord.hole_id = self.hole_id
            coord.data_set = self.data_set
            self.coord.append(coord)


class AcquireSurvey(Survey, Base):
    """
    Define spatialite columns for Survey definition
    """
    __tablename__ = "HOLESURVEY"

    hole_id = Column('HOLEID', String, primary_key=True)
    data_set = Column('PROJECTCODE', String, primary_key=True)
    surv_type = Column('SURVTYPE', String, primary_key=True)

    azimuth = Column("AZIMUTH", Float)
    dip = Column("DIP", Float)
    depth = Column("DEPTH", Float)
    loaded_by = ACQUIRE_PERSON_CODE
