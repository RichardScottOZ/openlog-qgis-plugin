from datetime import datetime
from typing import Any, List, Union

import numpy as np
import sqlalchemy as sa
from sqlalchemy import and_, or_
from sqlalchemy.orm import Session

from openlog.datamodel.assay.generic_assay import (
    AssayDatabaseDefinition,
    AssayDefinition,
    DepthAssay,
    TimeAssay,
)


class SqlAlchemyAssayReader:
    def __init__(
        self,
        hole_id: str,
        session: Session,
        assay_table: sa.Table,
        assay_definition: AssayDefinition,
        assay_database_definition: AssayDatabaseDefinition,
    ) -> None:
        self.hole_id = hole_id
        self._assay_table = assay_table
        self._session = session
        self._assay_definition = assay_definition
        self._assay_database_definition = assay_database_definition

    def _get_query_col(self, col_str):
        if col_str in self._assay_table.c.keys():
            return self._assay_table.c[col_str]
        else:
            return sa.column(col_str)

    def _add_remove_none_filter(self, filter_: Any, column: str) -> Any:
        return and_(
            filter_,
            # WARNING : Need to use  `!=` and not `is not` or sqlachemy returns none values
            self._get_query_col(column) != None,
        )

    def _add_y_column_filter_and_get_query_col(
        self, filter_: Any, column_: str
    ) -> (Any, Any):
        if column_ in self._assay_database_definition.y_column_filter:
            row_filter = self._assay_database_definition.y_column_filter[column_]
            filter_ = and_(
                filter_,
                sa.column(row_filter) == column_,
            )
            query_col = sa.column(self._assay_database_definition.y_col[column_])
        else:
            database_col = (
                self._assay_database_definition.y_col[column_]
                if column_ in self._assay_database_definition.y_col
                else column_
            )
            query_col = self._get_query_col(database_col)
        return filter_, query_col

    @property
    def hole_col(self) -> sa.Column:
        return self._assay_table.c[self._assay_database_definition.hole_id_col]

    @property
    def x_col(self) -> sa.Column:
        return self._assay_table.c[self._assay_database_definition.x_col]

    @property
    def x_end_col(self) -> sa.Column:
        return self._assay_table.c[self._assay_database_definition.x_end_col]


class SqlAlchemyDiscreteAssayReader(SqlAlchemyAssayReader):
    """
    Helper class to read discrete assay data from sqlalchemy session with a sqlalchemy table

    Args:
        hole_id: collar hole id for filter
        session: sqlalchemy session
        assay_table: sqlalchemy table class used for assay data request
    """

    def __init__(
        self,
        hole_id: str,
        session: Session,
        assay_table: sa.Table,
        assay_definition: AssayDefinition,
        assay_database_definition: AssayDatabaseDefinition,
    ) -> None:
        super().__init__(
            hole_id, session, assay_table, assay_definition, assay_database_definition
        )

    def get_all_values(
        self, column: str, remove_none: bool = False
    ) -> (np.array, np.array):
        """
        Returns all values available for assay column as numpy arrays

        Args:
            remove_none:
            column (str): assay column

        """
        filter_ = self.hole_col == self.hole_id
        if remove_none:
            filter_ = self._add_remove_none_filter(filter_, column)

        filter_, query_col = self._add_y_column_filter_and_get_query_col(
            filter_, column
        )

        all_values = (
            self._session.query(self.x_col, query_col)
            .filter(filter_)
            .order_by(self.x_col)
            .all()
        )
        return self._convert_to_array(all_values)

    def _get_values_from_interval(
        self,
        column: str,
        x_min: Union[float, datetime],
        x_max: Union[float, datetime],
        remove_none: bool = False,
    ) -> (np.array, np.array):
        """
        Query session with sqlalchemy table class with x interval filter

        Args:
            column (str): assay column
            x_min: x minimum value
            x_max: x maximum value
            remove_none: (bool) remove none values (default False)

        Returns: tuple (x_value,y_values) as numpy array

        """
        filter_ = and_(
            x_min <= self.x_col,
            self.x_col <= x_max,
            self.hole_col == self.hole_id,
        )
        if remove_none:
            filter_ = self._add_remove_none_filter(filter_, column)

        filter_, query_col = self._add_y_column_filter_and_get_query_col(
            filter_, column
        )

        all_values = (
            self._session.query(self.x_col, query_col)
            .filter(filter_)
            .order_by(self.x_col)
            .all()
        )
        return self._convert_to_array(all_values)

    @staticmethod
    def _convert_to_array(values: List) -> (np.array, np.array):
        """
        Convert result of query to numpy array

        Args:
            values: query result

        Returns: tuple (x_value,y_values) as numpy array

        """
        if values:
            full_array = np.array(values)
            # Discrete assay : x has 1 dimension
            return full_array[:, 0], full_array[:, 1]
        else:
            return np.array([]), np.array([])


class SqlAlchemyExtendedAssayReader(SqlAlchemyAssayReader):
    """
    Helper class to read extended assay data from sqlalchemy session with a sqlalchemy table


    Args:
        hole_id: collar hole id for filter
        session: sqlalchemy session
        assay_table: sqlalchemy table class used for assay data request
    """

    def __init__(
        self,
        hole_id: str,
        session: Session,
        assay_table: sa.Table,
        assay_definition: AssayDefinition,
        assay_database_definition: AssayDatabaseDefinition,
    ) -> None:
        super().__init__(
            hole_id, session, assay_table, assay_definition, assay_database_definition
        )

    def get_all_values(
        self, column: str, remove_none: bool = False
    ) -> (np.array, np.array):
        """
        Returns all values available for assay column as numpy arrays

        Args:
            column (str): assay column
            remove_none: (bool) remove none values (default False)

        """
        filter_ = self.hole_col == self.hole_id
        if remove_none:
            filter_ = self._add_remove_none_filter(filter_, column)

        filter_, query_col = self._add_y_column_filter_and_get_query_col(
            filter_, column
        )
        all_values = (
            self._session.query(
                self.x_col,
                self.x_end_col,
                query_col,
            )
            .filter(filter_)
            .order_by(self.x_col)
            .all()
        )
        return self._convert_to_array(all_values)

    def _get_values_from_interval(
        self,
        column: str,
        x_min: Union[float, datetime],
        x_max: Union[float, datetime],
        remove_none: bool = False,
    ) -> (np.array, np.array):
        """
        Query session with sqlalchemy table class with x interval filter

        Args:
            column (str): assay column
            x_min: x minimum value
            x_max: x maximum value
            remove_none: (bool) remove none values (default False)

        Returns: tuple (x_value,y_values) as numpy array
        """
        filter_ = and_(
            self.hole_col == self.hole_id,
            or_(
                and_(x_min <= self.x_col, self.x_col <= x_max),
                and_(
                    x_min <= self.x_end_col,
                    self.x_end_col <= x_max,
                ),
                and_(x_min <= self.x_col, self.x_end_col <= x_max),
                and_(x_min >= self.x_col, self.x_end_col >= x_max),
            ),
        )
        if remove_none:
            filter_ = self._add_remove_none_filter(filter_, column)

        filter_, query_col = self._add_y_column_filter_and_get_query_col(
            filter_, column
        )
        all_values = (
            self._session.query(
                self.x_col,
                self.x_end_col,
                query_col,
            )
            .filter(filter_)
            .order_by(self.x_col)
            .all()
        )
        return self._convert_to_array(all_values)

    @staticmethod
    def _convert_to_array(values: List) -> (np.array, np.array):
        """
        Convert result of query to numpy array

        Args:
            values: query result

        Returns: tuple (x_value,y_values) as numpy array

        """
        if values:
            full_array = np.array(values)
            # Extended assay : x has 2 dimension (min/max)
            return full_array[:, [0, 1]], full_array[:, 2]
        else:
            return np.array([]), np.array([])


class SqlAlchemyDiscreteDepthAssay(SqlAlchemyDiscreteAssayReader, DepthAssay):
    """
    DepthAssay with discrete x values using a sqlalchemy connection to read data

    Inherits SqlAlchemyDiscreteAssayReader to read discrete data

    Args:
        hole_id: collar hole id for filter
        assay_definition: AssayDefinition
        session: sqlalchemy session
        assay_table: sqlalchemy table class used for assay data request
    """

    def __init__(
        self,
        hole_id: str,
        assay_definition: AssayDefinition,
        assay_database_definition: AssayDatabaseDefinition,
        session: Session,
        assay_table: sa.Table,
    ) -> None:
        DepthAssay.__init__(self, hole_id, assay_definition)
        SqlAlchemyDiscreteAssayReader.__init__(
            self,
            hole_id,
            session,
            assay_table,
            assay_definition,
            assay_database_definition,
        )

    def get_values_from_depth(
        self, column: str, depth_min: float, depth_max: float, remove_none: bool = False
    ) -> (np.array, np.array):
        """
        Returns available values for assay column as numpy arrays from a depth interval

        Args:
            column (str): assay column
            depth_min (float): minimum depth
            depth_max (float): maximum depth
            remove_none: (bool) remove none values (default False)
        """
        return self._get_values_from_interval(column, depth_min, depth_max, remove_none)


class SqlAlchemyExtendedDepthAssay(SqlAlchemyExtendedAssayReader, DepthAssay):
    """
    DepthAssay with extended x values using a sqlalchemy connection to read data

    Inherits SqlAlchemyExtendedAssayReader to read extended data

    Args:
        hole_id: collar hole id for filter
        assay_definition: AssayDefinition
        session: sqlalchemy session
        assay_table: sqlalchemy table class used for assay data request
    """

    def __init__(
        self,
        hole_id: str,
        assay_definition: AssayDefinition,
        assay_database_definition: AssayDatabaseDefinition,
        session: Session,
        assay_table: sa.Table,
    ) -> None:
        DepthAssay.__init__(self, hole_id, assay_definition)
        SqlAlchemyExtendedAssayReader.__init__(
            self,
            hole_id,
            session,
            assay_table,
            assay_definition,
            assay_database_definition,
        )
        self._assay_table = assay_table
        self._session = session

    def get_values_from_depth(
        self, column: str, depth_min: float, depth_max: float, remove_none: bool = False
    ) -> (np.array, np.array):
        """
        Returns available values for assay column as numpy arrays from a depth interval

        Args:
            column (str): assay column
            depth_min (float): minimum depth
            depth_max (float): maximum depth
            remove_none: (bool) remove none values (default False)
        """
        return self._get_values_from_interval(column, depth_min, depth_max, remove_none)


class SqlAlchemyDiscreteTimeAssay(SqlAlchemyDiscreteAssayReader, TimeAssay):
    """
    DepthAssay with discrete x values using a sqlalchemy connection to read data

    Inherits SqlAlchemyDiscreteAssayReader to read discrete data

    Args:
        hole_id: collar hole id for filter
        assay_definition: AssayDefinition
        session: sqlalchemy session
        assay_table: sqlalchemy table class used for assay data request
    """

    def __init__(
        self,
        hole_id: str,
        assay_definition: AssayDefinition,
        assay_database_definition: AssayDatabaseDefinition,
        session: Session,
        assay_table: sa.Table,
    ) -> None:
        TimeAssay.__init__(self, hole_id, assay_definition)
        SqlAlchemyDiscreteAssayReader.__init__(
            self,
            hole_id,
            session,
            assay_table,
            assay_definition,
            assay_database_definition,
        )
        self._assay_table = assay_table
        self._session = session

    def get_values_from_time(
        self,
        column: str,
        date_min: datetime,
        date_max: datetime,
        remove_none: bool = False,
    ) -> (np.array, np.array):
        """
        Returns available values for assay column as numpy arrays from a time interval.

        Args:
            column (str): assay column
            date_min (datetime): minimum datetime
            date_max (datetime): maximum datetime
            remove_none: (bool) remove none values (default False)

        """
        return self._get_values_from_interval(column, date_min, date_max, remove_none)


class SqlAlchemyExtendedTimeAssay(SqlAlchemyExtendedAssayReader, TimeAssay):
    """
    TimeAssay with extended x values using a sqlalchemy connection to read data

    Inherits SqlAlchemyExtendedAssayReader to read extended data

    Args:
        hole_id: collar hole id for filter
        assay_definition: AssayDefinition
        session: sqlalchemy session
        assay_table: sqlalchemy table class used for assay data request
    """

    def __init__(
        self,
        hole_id: str,
        assay_definition: AssayDefinition,
        assay_database_definition: AssayDatabaseDefinition,
        session: Session,
        assay_table: sa.Table,
    ):
        TimeAssay.__init__(self, hole_id, assay_definition)
        SqlAlchemyExtendedAssayReader.__init__(
            self,
            hole_id,
            session,
            assay_table,
            assay_definition,
            assay_database_definition,
        )
        self._assay_table = assay_table
        self._session = session

    def get_values_from_time(
        self,
        column: str,
        date_min: datetime,
        date_max: datetime,
        remove_none: bool = False,
    ) -> (np.array, np.array):
        """
        Returns available values for assay column as numpy arrays from a time interval.

        Args:
            column (str): assay column
            date_min (datetime): minimum datetime
            date_max (datetime): maximum datetime
            remove_none: (bool) remove none values (default False)

        """
        return self._get_values_from_interval(column, date_min, date_max, remove_none)
