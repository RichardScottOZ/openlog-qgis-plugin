from xplordb.datamodel.collar import Collar
from xplordb.datamodel.dataset import Dataset
from xplordb.datamodel.lith import Lith
from xplordb.datamodel.person import Person
from xplordb.datamodel.survey import Survey
from xplordb.import_data import ImportData

from openlog.datamodel.connection.interfaces.read_interface import ReadInterface
from openlog.datamodel.connection.interfaces.write_interface import WriteInterface


class XplordbWriteInterface(WriteInterface):
    def __init__(self, session, read_iface: ReadInterface):
        """
        Implement WriteInterface with a xplordb session

        Args:
            session: sqlalchemy session created from engine
        """
        super(XplordbWriteInterface, self).__init__()
        self.session = session
        self._read_iface = read_iface

    def can_import_collar(self) -> bool:
        """
        Indicate if connection can import collar

        Returns: True

        """
        return True

    def import_collar(self, collars: [Collar]) -> None:
        """
        Import collar into xplordb database.

        Raises WriteInterface.ImportException on import failure

        Args:
            collars: collars to be imported
        """
        try:
            import_data = ImportData(self.session)
            import_data.import_collars_array(collars)
        except ImportData.ImportException as exc:
            self.session.rollback()
            raise WriteInterface.ImportException(exc)

    def import_datasets(self, datasets: [Dataset]) -> None:
        """
        Import dataset into xplordb database.

        Raises WriteInterface.ImportException on import failure

        Args:
            datasets: datasets to be imported
        """
        try:
            import_data = ImportData(self.session)
            import_data.import_datasets_array(datasets)
        except ImportData.ImportException as exc:
            self.session.rollback()
            raise WriteInterface.ImportException(exc)

    def person_code_max_size(self) -> int:
        """
        Define maximum size for person code creation

        Returns: 3

        """
        return 3

    def import_persons(self, persons: [Person]) -> None:
        """
        Import persons into xplordb database.

        Raises WriteInterface.ImportException on import failure

        Args:
            persons: persons to be imported
        """
        try:
            import_data = ImportData(self.session)
            import_data.import_persons_array(persons)
        except ImportData.ImportException as exc:
            self.session.rollback()
            raise WriteInterface.ImportException(exc)

    def import_surveys(self, surveys: [Survey]) -> None:
        """
        Import survey into xplordb database.

        Raises WriteInterface.ImportException on import failure

        Args:
            surveys: surveys to be imported
        """

        try:
            import_data = ImportData(self.session)
            import_data.import_surveys_array(surveys)
        except ImportData.ImportException as exc:
            self.session.rollback()
            raise WriteInterface.ImportException(exc)

    def import_liths(self, liths: [Lith]) -> None:
        """
        Import lithologies into xplordb database.

        Raises WriteInterface.ImportException on import failure

        Args:
            liths: lithologies to be imported
        """

        try:
            import_data = ImportData(self.session)
            import_data.import_liths_array(liths)
        except ImportData.ImportException as exc:
            self.session.rollback()
            raise WriteInterface.ImportException(exc)

    def selected_collar_survey_definition_available(self) -> bool:
        """
        Return True if the collar survey definition is available
        :return: A boolean value.
        """
        return True

    def replace_collar_surveys(self, collar_id: str, surveys: [Survey]) -> None:
        """
        Replace survey for collar

        Args:
            collar_id:: The collar id of the hole
            surveys: new survey for collar
        """
        current_survey = self._read_iface.get_surveys_from_collars([collar_id])
        for survey in current_survey:
            self.session.delete(survey)
        self.session.flush()
        self.import_surveys(surveys)
