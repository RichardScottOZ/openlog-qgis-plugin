from qgis.core import QgsDataSourceUri, QgsVectorLayer

from openlog.datamodel.connection.interfaces.layers_interface import LayersInterface
from openlog.datamodel.connection.openlog_connection import Connection
from openlog.toolbelt import PlgTranslator


class XplordbLayersInterface(LayersInterface):
    def __init__(self, connection: Connection):
        """
        Implements LayersInterface for XplordbConnection

        Args:
            connection: xplordb connection parameters
        """
        super().__init__()
        self._connection = connection
        self.tr = PlgTranslator().tr

    def get_collar_layer(self) -> QgsVectorLayer:
        """
        Return collar QgsVectorLayer

        In xplordb collar geometry is available in dh.collar geom column

        """
        if self.collar_layer is None:
            uri = self._get_datasource_uri()
            uri.setDataSource("dh", "collar", "geom")
            self.collar_layer = QgsVectorLayer(
                uri.uri(False), self.get_collar_layer_name(), "postgres"
            )
        return self.collar_layer

    def get_collar_layer_name(self) -> str:
        """
        Get collar layer name

        Returns: (str) collar layer name

        """
        return self.tr(f"Collar - [{self._connection.database}]")

    def get_collar_trace_layer(self) -> QgsVectorLayer:
        """
        Return collar trace QgsVectorLayer

        In xplordb collar trace geometry is available in dh.collar geom_trace column

        """
        if self.collar_trace_layer is None:
            uri = self._get_datasource_uri()
            uri.setDataSource("dh", "collar", "geom_trace")
            self.collar_trace_layer = QgsVectorLayer(
                uri.uri(False), self.get_collar_trace_layer_name(), "postgres"
            )
        return self.collar_trace_layer

    def get_collar_trace_layer_name(self) -> str:
        """
        Get collar trace layer name

        Returns: (str) collar trace layer name

        """
        return self.tr(f"Trace - [{self._connection.database}]")

    def _get_datasource_uri(self) -> QgsDataSourceUri:
        """
        Get a QgsDataSourceUri from current connection parameters

        Returns: QgsDataSourceUri for with current connection parameters

        """
        uri = QgsDataSourceUri()
        uri.setConnection(
            aHost=self._connection.host,
            aPort=str(self._connection.port),
            aDatabase=self._connection.database,
            aUsername=self._connection.user,
            aPassword=self._connection.password,
        )
        return uri
