from pathlib import Path

from qgis.core import QgsDataSourceUri, QgsVectorLayer

from openlog.__about__ import DIR_PLUGIN_ROOT
from openlog.datamodel.connection.interfaces.layers_interface import LayersInterface
from openlog.datamodel.connection.openlog_connection import Connection
from openlog.toolbelt import PlgTranslator


class BDGeoLayersInterface(LayersInterface):
    def __init__(self, connection: Connection):
        """
        Implements LayersInterface for BDGeoConnection

        Args:
            connection:  Connection
        """
        super().__init__()
        self._connection = connection
        self.tr = PlgTranslator().tr

    def get_collar_layer(self) -> QgsVectorLayer:
        """
        Return collar QgsVectorLayer

        In spatialite collar geometry is available in collar geom column

        """
        if self.collar_layer is None:
            uri = self._get_datasource_uri()
            uri.setDataSource("qgis", "station", "point", "", "id")
            self.collar_layer = QgsVectorLayer(uri.uri(False), "collar", "postgres")
            self.collar_layer.setReadOnly(True)
            self.collar_layer.setName(self.get_collar_layer_name())
        return self.collar_layer

    def get_collar_layer_name(self) -> str:
        """
        Get collar layer name

        Returns: (str) collar layer name

        """
        return self.tr(f"Stations - [{self._connection.database}]")

    def get_collar_layer_style_file(self) -> Path:
        """
        Get QGIS file style for collar layer

        Returns: path to QGIS style file

        """
        return DIR_PLUGIN_ROOT / "resources" / "styles" / "collar_bdgeo.qml"

    def get_selected_collar_from_layer(self) -> [str]:
        """
        Get selected collar id from QGIS layer

        Returns: [str] selected collar id

        """
        collar_layer = self.get_collar_layer()
        if collar_layer is not None:
            res = [str(f["id"]) for f in collar_layer.selectedFeatures()]
        else:
            res = []
        return res

    def get_collar_trace_layer(self) -> QgsVectorLayer:
        """
        Return None for collar trace QgsVectorLayer :

        """
        return None

    def get_collar_trace_layer_name(self) -> str:
        """
        Get collar trace layer name

        Returns: (str) collar trace layer name

        """
        return ""

    def _get_datasource_uri(self) -> QgsDataSourceUri:
        """
        Get a QgsDataSourceUri from current connection parameters

        Returns: QgsDataSourceUri for with current connection parameters

        """
        uri = QgsDataSourceUri()
        uri.setConnection(
            aHost=self._connection.host,
            aPort=str(self._connection.port),
            aDatabase=self._connection.database,
            aUsername=self._connection.user,
            aPassword=self._connection.password,
        )
        return uri
