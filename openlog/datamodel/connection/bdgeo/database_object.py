from geoalchemy2 import Geometry
from sqlalchemy import Column, Integer, String
from sqlalchemy.dialects.postgresql import REGCLASS
from sqlalchemy.orm import declarative_base
from xplordb.datamodel.collar import Collar

from openlog.datamodel.assay.generic_assay import (
    AssayColumn,
    AssayDatabaseDefinition,
    AssayDataExtent,
    AssayDefinition,
    AssayDomainType,
    AssaySeriesType,
)

Base = declarative_base()

DEFAULT_SRID = 3857
DBGEO_PERSON_CODE: str = "dbgeo"
DBGEO_DATASET: str = "dbgeo_dataset"


class MeasureMetadata(Base):
    """
    Define dbgeo columns for measure metadata, used to define available Assay
    """

    __tablename__ = "measure_metadata"
    __table_args__ = {"schema": "measure"}

    measure_table = Column("measure_table", REGCLASS, primary_key=True)
    name = Column("name", String)
    unit = Column("unit_of_measure", String)
    x_axis_type = Column("x_axis_type", String)
    storage_type = Column("storage_type", String)
    measure_category = Column("measure_category", String)

    def get_chemical_element(self, session) -> [str]:
        chemical_elements = []

        # Check if chemical_element columns available
        keys = session.execute(f"SELECT * FROM {self.measure_table} LIMIT 1").keys()
        if "chemical_element" in keys:
            chemical_elements = [
                res[0]
                for res in session.execute(
                    f"SELECT DISTINCT chemical_element FROM {self.measure_table}"
                )
            ]
        return chemical_elements

    def to_assay_definition(self, session) -> AssayDefinition:
        chemical_elements = self.get_chemical_element(session)

        if len(chemical_elements) == 0:
            columns = {
                self.name: AssayColumn(
                    name=self.name,
                    series_type=AssaySeriesType.NUMERICAL,
                    unit=self.unit,
                )
            }
        else:
            columns = {}
            for elem in chemical_elements:
                columns[elem] = AssayColumn(
                    elem, series_type=AssaySeriesType.NUMERICAL, unit=self.unit
                )
        domain = (
            AssayDomainType.DEPTH
            if self.x_axis_type == "DepthAxis"
            else AssayDomainType.TIME
        )
        data_extent = (
            AssayDataExtent.EXTENDED
            if self.storage_type == "Cumulative"
            else AssayDataExtent.DISCRETE
        )

        return AssayDefinition(
            variable=self.measure_table,
            domain=domain,
            data_extent=data_extent,
            columns=columns,
            display_name=self.name,
        )

    def to_assay_database_definition(self, session) -> AssayDatabaseDefinition:
        table_name = f"measure_{self.measure_table.replace('measure.', '')}"
        hole_id_col = "station_id"
        dataset_col = "dataset_id"

        domain = (
            AssayDomainType.DEPTH
            if self.x_axis_type == "DepthAxis"
            else AssayDomainType.TIME
        )
        data_extent = (
            AssayDataExtent.EXTENDED
            if self.storage_type == "Cumulative"
            else AssayDataExtent.DISCRETE
        )

        # Should be changed in BDGeo views, different columns for extended assay in depth and time domain
        if domain == AssayDomainType.DEPTH and data_extent == AssayDataExtent.EXTENDED:
            y_column_name = "value"
        else:
            y_column_name = "measure_value"

        chemical_elements = self.get_chemical_element(session)

        if len(chemical_elements) == 0:
            y_col = {self.name: y_column_name}
            y_column_filter = {}
        else:
            y_col = {}
            y_column_filter = {}
            for elem in chemical_elements:
                y_col[elem] = y_column_name
                y_column_filter[elem] = "chemical_element"

        if data_extent == AssayDataExtent.EXTENDED:
            x_col = (
                "start_measure_time" if domain == AssayDomainType.TIME else "depth_from"
            )
            x_end_col = (
                "end_measure_time" if domain == AssayDomainType.TIME else "depth_to"
            )
        else:
            x_col = (
                "measure_time" if domain == AssayDomainType.TIME else "measure_depth"
            )
            x_end_col = ""

        schema = "qgis"

        return AssayDatabaseDefinition(
            table_name=table_name,
            hole_id_col=hole_id_col,
            dataset_col=dataset_col,
            x_col=x_col,
            y_col=y_col,
            y_column_filter=y_column_filter,
            x_end_col=x_end_col,
            schema=schema,
        )


class BDGeoCollar(Collar, Base):
    """
    Define dbgeo columns for Collar definition
    """

    __tablename__ = "station"
    __table_args__ = {"schema": "qgis"}

    hole_id = Column("id", String, primary_key=True)
    name = Column("name", String)
    data_set = Column("dataset_id", String)
    srid = Column("orig_srid", Integer)
    loaded_by = DBGEO_PERSON_CODE
    geom = Column(
        "point",
        Geometry(
            geometry_type="POINT", dimension=3, management=True, srid=DEFAULT_SRID
        ),
    )

    @property
    def x(self) -> float:
        return self.geom.ST_X()

    @property
    def y(self) -> float:
        return self.geom.ST_Y()

    @property
    def z(self) -> float:
        return self.geom.ST_Z()
