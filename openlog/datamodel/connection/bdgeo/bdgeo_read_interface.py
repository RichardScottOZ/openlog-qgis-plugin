from openlog.datamodel.connection.bdgeo.database_object import BDGeoCollar
from openlog.datamodel.connection.interfaces.read_interface import ReadInterface


class BDGeoReadInterface(ReadInterface):
    def __init__(self, session):
        """
        Implements ReadInterface for BDGeoConnection

        Args:
            session: sqlalchemy session for BDGeo
        """
        super().__init__()
        self.session = session

    def get_collar_display_name(self, hole_id: str) -> str:
        """
        Get collar display name from hole_id. Get name from bdgeo database

        Args:
            hole_id: str

        Returns: station name

        """
        return (
            self.session.query(BDGeoCollar)
            .filter(BDGeoCollar.hole_id == hole_id)
            .first()
            .name
        )
