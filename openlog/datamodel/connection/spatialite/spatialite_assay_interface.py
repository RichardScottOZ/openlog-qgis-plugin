from openlog.datamodel.assay.generic_assay import (
    AssayColumn,
    AssayDatabaseDefinition,
    AssayDataExtent,
    AssayDefinition,
    AssayDomainType,
    AssaySeriesType,
)
from openlog.datamodel.connection.interfaces.categories_interface import (
    CategoriesInterface,
)
from openlog.datamodel.connection.sqlalchemy.sqlalchemy_assay_interface import (
    SqlAlchemyAssayInterface,
)


class SpatialiteAssayInterface(SqlAlchemyAssayInterface):
    def __init__(self, engine, session, categories_iface: CategoriesInterface):
        """
        Implement AssayInterface for a Spatialite db

        Args:
            engine: sqlalchemy engine
            session: sqlalchemy session created from engine
            categories_iface : CategoriesInterface to get default lith category name
        """
        self._categories_iface = categories_iface
        super().__init__(engine, session)

    def get_lith_assay_definition(self) -> AssayDefinition:
        """
        Return AssayDefinition for lith assay in connection

        """
        return AssayDefinition(
            variable="lith",
            data_extent=AssayDataExtent.EXTENDED,
            domain=AssayDomainType.DEPTH,
            columns={
                "lith_code": AssayColumn(
                    name="lith_code",
                    series_type=AssaySeriesType.CATEGORICAL,
                    category_name=self._categories_iface.get_lith_category_name(),
                )
            },
        )

    def get_lith_assay_database_definition(self) -> AssayDatabaseDefinition:
        """
        Return AssayDatabaseDefinition for lith assay in connection

        """
        assay_database_definition = AssayDatabaseDefinition(
            table_name="lith",
            hole_id_col="hole_id",
            dataset_col="dataset",
            x_col="from_m",
            x_end_col="to_m",
            y_col={"lith_code": "lith_code"},
        )
        return assay_database_definition
