import os

from qgis.PyQt import uic
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QDialog


class CheckableComboBoxInputDialog(QDialog):
    def __init__(
        self,
        available_values: [str],
        display_values: [str],
        parent=None,
        title: str = "",
        label: str = "",
    ) -> None:
        """
        QDialog to select values for list

        Args:
            available_values : [str] list of available value
            display_values: [str] list of display value
            parent: QDialog parent
            title: QDialog title
            label : label
        """
        super().__init__(parent)
        uic.loadUi(
            os.path.join(
                os.path.dirname(__file__), "checkable_combobox_input_dialog.ui"
            ),
            self,
        )

        self.setWindowTitle(title)
        self.label.setText(label)

        for i in range(0, len(available_values)):
            self.value_check_combobox.addItemWithCheckState(
                display_values[i], Qt.Checked, available_values[i]
            )

    def clear_selection(self) -> None:
        """
        Clear selected items

        """
        self.value_check_combobox.deselectAllOptions()

    def set_selected_values(
        self, available_values: [str], display_values: [str]
    ) -> None:
        """
        Define selected value
        Args:
            available_values: [str] selected value
            display_values: [str] display selected value
        """
        for i in range(0, len(available_values)):
            self.value_check_combobox.addItemWithCheckState(
                display_values[i], Qt.Checked, available_values[i]
            )

    def get_selected_values(self) -> [str]:
        """
        Return selected value

        Returns: [str] selected values

        """
        return self.value_check_combobox.checkedItemsData()
