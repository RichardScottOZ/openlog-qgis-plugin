import pyqtgraph as pq
from pyqtgraph import mkPen, mkBrush


class CustomBarGraphItem(pq.BarGraphItem):

    def __init__(self, **opts):
        """
        Override pg.BarGraphItem to add setPen and setBrush methods

        Args:
            **opts:
        """
        pq.BarGraphItem.__init__(self, **opts)

    def setPen(self, *args, **kargs):
        """
        Sets the pen used to draw graph line.
        The argument can be a :class:`QtGui.QPen` or any combination of arguments accepted by
        :func:`pyqtgraph.mkPen() <pyqtgraph.mkPen>`.
        """
        pen = mkPen(*args, **kargs)
        self.opts['pen'] = pen
        self.picture = None

    def setBrush(self, *args, **kargs):
        """
        Sets the brush used to draw bar rect.
        The argument can be a :class:`QBrush` or argument to :func:`~pyqtgraph.mkBrush`
        """
        brush = mkBrush(*args, **kargs)
        self.opts['brush'] = brush
        self.picture = None
