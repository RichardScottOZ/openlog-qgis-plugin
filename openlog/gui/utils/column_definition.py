from dataclasses import dataclass

from openlog.datamodel.assay.generic_assay import AssaySeriesType


@dataclass
class ColumnDefinition:
    """Class for column definition

    Args:
        column (str): column name
        mapping (str): column mapping in delimited text
        unit (str): column unit
        fixed (bool): is column fixed (can't be removed)
        optional (bool): is column optional (must be defined for import)
        series_type (AssaySeriesType): series type
        category_name (str): category name
    """

    column: str
    mapping: str = ""
    unit: str = ""
    fixed: bool = False
    optional: bool = False
    series_type: AssaySeriesType = AssaySeriesType.NUMERICAL
    category_name: str = ""
