import pandas
from qgis.gui import QgsCollapsibleGroupBox
from qgis.PyQt.QtWidgets import (
    QButtonGroup,
    QGridLayout,
    QLabel,
    QRadioButton,
    QTableWidget,
    QVBoxLayout,
    QWidget,
    QWizardPage,
)
from xplordb.datamodel.lith import Lith

from openlog.core.lithology_interpolation import (
    GapDataResolution,
    LithologyColumn,
    LithologyInterpolation,
    OverlapDataResolution,
)
from openlog.datamodel.assay.generic_assay import AssaySeriesType
from openlog.datamodel.connection.openlog_connection import OpenLogConnection
from openlog.gui.utils.column_definition import ColumnDefinition
from openlog.gui.utils.delimited_text_import_widget import DelimitedTextImportWidget
from openlog.toolbelt.translator import PlgTranslator

BASE_SETTINGS_KEY = "/OpenLog/gui/import/liths"


class LithsDelimitedTextImportWidget(DelimitedTextImportWidget):
    def __init__(
        self, parent=None, openlog_connection: OpenLogConnection = None
    ) -> None:
        """
        Override DelimitedTextImportWidget get_dataframe method to get interpolated
        lithography data with gap and overlap

        Args:
            parent: QWidget parent
        """
        self.UPDATE_STATUS_COL = "update_status"
        super(LithsDelimitedTextImportWidget, self).__init__(parent, openlog_connection)

    def get_dataframe(self, nb_row: int = None) -> pandas.DataFrame:
        """
        Override DelimitedTextImportWidget to interpolate gap and overlap data

        Args:
            nb_row: read only a specific number of row (default None : all rows are read)

        Returns:
            panda dataframe with expected column and interpolated data

        """
        df = super().get_dataframe(nb_row)

        if df is not None:
            interpolation = LithologyInterpolation(
                gap_resolution=self.parent().get_gap_data_resolution(),
                overlap_resolution=self.parent().get_overlap_data_resolution(),
                lith_columns=LithologyColumn(
                    hole_id_col=self.parent().HOLE_ID_COL,
                    from_col=self.parent().FROM_COL,
                    to_col=self.parent().TO_COL,
                    lith_code_col=self.parent().LITH_CODE_COL,
                    update_status_col=self.UPDATE_STATUS_COL,
                ),
            )
            try:
                df = interpolation.interpolated_dataframe(df)
                self.data_is_valid = True
            except LithologyInterpolation.InvalidColumnData:
                self.data_is_valid = False
        return df

    def _update_table_content(
        self, table_widget: QTableWidget, array: pandas.DataFrame
    ) -> None:
        """
        Override DelimitedTextImportWidget to display status of interpolation

        Args:
            table_widget: QTableWidget used of display
            array: Dataframe to display in QTableWidget
        """
        super()._update_table_content(table_widget, array)

        if self.UPDATE_STATUS_COL in array.columns:
            # Update item style
            for j in range(0, table_widget.columnCount()):
                i = 0
                for index, r in array.iterrows():
                    if r[self.UPDATE_STATUS_COL]:
                        font = table_widget.item(i, j).font()
                        font.setBold(True)
                        table_widget.item(i, j).setFont(font)
                    i = i + 1

            # Don't show update status col (last col)
            table_widget.setColumnHidden(table_widget.columnCount() - 1, True)


class LithsImportPageWizard(QWizardPage):
    def __init__(self, parent: QWidget, openlog_connection: OpenLogConnection) -> None:
        """
        QWizard to import lithologies into xplordb from csv file

        Args:
            openlog_connection: OpenLogConnection used to import lithologies
            parent : QWidget parent
        """
        super().__init__(parent)
        self.tr = PlgTranslator().tr
        self._openlog_connection = openlog_connection
        self.setTitle(self.tr("Lithology import"))

        self.HOLE_ID_COL = self.tr("HoleID")
        self.LITH_CODE_COL = self.tr("LithCode")
        self.FROM_COL = self.tr("From_m")
        self.TO_COL = self.tr("To_m")

        label = QLabel(self.tr("Select a .csv file for lithology import"))
        label.setWordWrap(True)

        layout = QVBoxLayout()
        layout.addWidget(label)

        # Add import option group box
        gb = QgsCollapsibleGroupBox(self.tr("Import options"), self)
        layout_gb = QGridLayout()
        gb.setLayout(layout_gb)

        self.gap_option_bg = QButtonGroup(self)
        self.gap_option = {}
        col = 0
        layout_gb.addWidget(QLabel(self.tr("Gap resolution")), 0, col)
        col = col + 1
        for o in GapDataResolution:
            rb = QRadioButton(self.tr(o.value), self)
            self.gap_option[o] = rb
            self.gap_option_bg.addButton(rb)
            layout_gb.addWidget(rb, 0, col)
            col = col + 1

        self.gap_option[GapDataResolution.ACCEPT].setChecked(True)

        self.overlap_option_bg = QButtonGroup(self)
        self.overlap_option = {}
        col = 0
        layout_gb.addWidget(QLabel(self.tr("Overlap resolution")), 1, col)
        col = col + 1
        for o in OverlapDataResolution:
            rb = QRadioButton(self.tr(o.value), self)
            self.overlap_option[o] = rb
            self.overlap_option_bg.addButton(rb)
            layout_gb.addWidget(rb, 1, col)
            col = col + 1

        self.overlap_option[OverlapDataResolution.REJECT].setChecked(True)

        gb.setLayout(layout_gb)
        layout.addWidget(gb)

        self.dataset_edit = LithsDelimitedTextImportWidget(
            parent=self, openlog_connection=self._openlog_connection
        )
        self.dataset_edit.set_column_definition(
            [
                ColumnDefinition(
                    column=self.HOLE_ID_COL,
                    fixed=True,
                    series_type=AssaySeriesType.NOMINAL,
                ),
                ColumnDefinition(
                    column=self.LITH_CODE_COL,
                    fixed=True,
                    series_type=AssaySeriesType.CATEGORICAL,
                    category_name=openlog_connection.get_categories_iface().get_lith_category_name(),
                ),
                ColumnDefinition(
                    column=self.FROM_COL,
                    unit="m",
                    fixed=True,
                    series_type=AssaySeriesType.NUMERICAL,
                ),
                ColumnDefinition(
                    column=self.TO_COL,
                    unit="m",
                    fixed=True,
                    series_type=AssaySeriesType.NUMERICAL,
                ),
            ]
        )

        layout.addWidget(self.dataset_edit)
        self.setLayout(layout)

        self.gap_option_bg.buttonClicked.connect(self.dataset_edit.update_result_table)
        self.overlap_option_bg.buttonClicked.connect(
            self.dataset_edit.update_result_table
        )

        self.dataset_edit.restore_settings(BASE_SETTINGS_KEY)

    def get_gap_data_resolution(self) -> GapDataResolution:
        for o in GapDataResolution:
            if self.gap_option[o].isChecked():
                return o

    def get_overlap_data_resolution(self) -> OverlapDataResolution:
        for o in OverlapDataResolution:
            if self.overlap_option[o].isChecked():
                return o

    def data_label(self) -> str:
        """
        Returns label to be used in confirmation dialog

        Returns: imported data label

        """
        return self.tr("Lithologies")

    def data_count(self) -> int:
        """
        Returns expected imported data count to be displayed in confirmation dialog

        Returns: expected imported data count

        """
        df = self.dataset_edit.get_dataframe()
        return df.shape[0] if df is not None else 0

    def import_data(self):
        """
        Import data into openlog database.

        OpenLogConnection.ImportData exception can be raised.

        """
        df = self.dataset_edit.get_dataframe()
        if df is not None:
            liths = [
                Lith(
                    hole_id=r[self.HOLE_ID_COL],
                    data_set=self.field("dataset"),
                    loaded_by=self.field("person"),
                    from_m=r[self.FROM_COL],
                    to_m=r[self.TO_COL],
                    lith_code=r[self.LITH_CODE_COL],
                )
                for index, r in df.iterrows()
            ]
            self._openlog_connection.get_write_iface().import_liths(liths)

    def validatePage(self) -> bool:
        """
        Validate current page content

        Returns: True if no data defined or if column content is validated

        """
        self.dataset_edit.save_setting(BASE_SETTINGS_KEY)
        return self.dataset_edit.data_is_valid
