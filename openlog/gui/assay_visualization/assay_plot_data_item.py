import pyqtgraph as pg

from openlog.datamodel.assay.generic_assay import AssayDomainType
from openlog.gui.assay_visualization.assay_visualization_config import (
    AssayColumnVisualizationConfig,
)


class AssayPlotDataItem(pg.PlotDataItem):
    def __init__(
        self,
        domain: AssayDomainType,
        config: AssayColumnVisualizationConfig,
        *args,
        **kargs
    ):
        super().__init__(*args, **kargs)
        self.domain = domain
        self.config = config
