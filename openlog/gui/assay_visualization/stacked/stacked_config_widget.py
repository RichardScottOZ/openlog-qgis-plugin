import os

import pyqtgraph.parametertree as ptree
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QWidget

from openlog.gui.assay_visualization.stacked.stacked_config import StackedConfiguration


class StackedConfigWidget(QWidget):
    def __init__(self, parent: QWidget = None):
        """
        Widget for StackedConfiguration display and edition.
        User can change assay columns configuration.

        Args:
            stacked_config: StackedConfiguration to be displayed
            parent: parent object
        """
        super().__init__(parent)
        self.setMinimumWidth(300)

        uic.loadUi(
            os.path.join(os.path.dirname(__file__), "stacked_config_widget.ui"),
            self,
        )

        # Add parameter tree
        self.parameter_tree = ptree.ParameterTree(self)
        self.param_layout.addWidget(self.parameter_tree)

    def set_stacked_config(self, stacked_config: StackedConfiguration) -> None:
        """
        Define parameter for a stacked configuration

        Args:
            stacked_config: (StackedConfiguration)
        """

        self.stacked_label_values.setText(stacked_config.name)

        self.parameter_tree.clear()

        # Define params for all assay column
        all_params = ptree.Parameter.create(name="Columns", type="group")

        for config in stacked_config.config_list:
            all_params.addChild(config.get_pyqtgraph_param())

        self.parameter_tree.setParameters(all_params, showTop=False)

        for config in stacked_config.config_list:
            for w in config.create_configuration_widgets(self):
                self.param_layout.addWidget(w)
