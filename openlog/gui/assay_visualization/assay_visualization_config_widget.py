import os

import pyqtgraph.parametertree as ptree
from qgis.PyQt import QtCore, uic
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction, QMenu, QWidget

from openlog.__about__ import DIR_PLUGIN_ROOT
from openlog.gui.assay_visualization.assay_visualization_config import (
    AssayVisualizationConfig,
)


class AssayVisualizationConfigWidget(QWidget):
    apply_to_all = QtCore.pyqtSignal()
    apply_to_visible = QtCore.pyqtSignal()
    apply_to_selected = QtCore.pyqtSignal()

    def __init__(
        self,
        parent: QWidget = None,
    ):
        """
        Widget for AssayVisualizationConfig display and edition.
        User can change assay columns configuration.

        Args:
            parent: parent object
        """
        super().__init__(parent)
        self.assay_config = None
        self.setMinimumWidth(300)

        uic.loadUi(
            os.path.join(
                os.path.dirname(__file__), "assay_visualization_config_widget.ui"
            ),
            self,
        )

        self.assay_config = None

        # Add parameter tree
        self.parameter_tree = ptree.ParameterTree(self)
        self.parameter_tree.setHeaderLabels([self.tr("Parameter"), self.tr("Value")])
        self.param_layout.addWidget(self.parameter_tree)

        menu = QMenu(self)

        self.apply_to_all_action = QAction(
            QIcon(str(DIR_PLUGIN_ROOT / "resources" / "images" / "apply_all.svg")),
            self.tr("Apply to all"),
            self,
        )
        self.apply_to_all_action.triggered.connect(lambda: self.apply_to_all.emit())
        menu.addAction(self.apply_to_all_action)

        self.apply_to_visible_action = QAction(
            QIcon(str(DIR_PLUGIN_ROOT / "resources" / "images" / "apply_visible.svg")),
            self.tr("Apply to visible"),
            self,
        )
        self.apply_to_visible_action.triggered.connect(
            lambda: self.apply_to_visible.emit()
        )
        menu.addAction(self.apply_to_visible_action)

        self.apply_to_selected_action = QAction(
            QIcon(
                str(DIR_PLUGIN_ROOT / "resources" / "images" / "apply_selection.svg")
            ),
            self.tr("Apply to selected"),
            self,
        )
        self.apply_to_selected_action.triggered.connect(
            lambda: self.apply_to_selected.emit()
        )
        menu.addAction(self.apply_to_selected_action)

        self.apply_button.setMenu(menu)

    def set_assay_config(self, assay_config: AssayVisualizationConfig) -> None:
        """
        Define parameter for an assay configuration

        Args:
            assay_config: (AssayVisualizationConfig)
        """
        self.assay_config = assay_config

        self.assay_label_values.setText(assay_config.assay_display_name)
        if assay_config.hole_id:
            self.collar_label_values.setText(assay_config.hole_display_name)
        else:
            self.collar_label.setVisible(False)
            self.collar_label_values.setVisible(False)

        self.parameter_tree.clear()

        # Define params for all assay column
        all_params = ptree.Parameter.create(name=self.tr("Columns"), type="group")
        all_params.addChild(assay_config.get_pyqtgraph_param())
        for col, assay_column_config in assay_config.column_config.items():
            all_params.addChild(assay_column_config.get_pyqtgraph_param())

        self.parameter_tree.setParameters(all_params, showTop=False)

        for assay_column_config in assay_config.column_config.values():
            for w in assay_column_config.create_configuration_widgets(self):
                self.param_layout.addWidget(w)
