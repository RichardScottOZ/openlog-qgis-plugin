import pyqtgraph as pg
import pyqtgraph.parametertree as ptree
import pyqtgraph.parametertree.parameterTypes as pTypes

from openlog.datamodel.assay.generic_assay import AssayDomainType
from openlog.datamodel.assay.uncertainty import UncertaintyType
from openlog.gui.assay_visualization.assay_visualization_config import (
    AssayColumnVisualizationConfig,
)


class DiscreteAssayColumnVisualizationConfig(AssayColumnVisualizationConfig):
    def __init__(self, column: str):
        """
        Store visualization configuration for a discrete assay column.
        Can also access plot item if configuration created from visualization widget

        Configuration supported :
        - visibility_param (bool) : assay column visibility (default : True)
        - pen_params (QPen) : assay column pen (default : black)
        - uncertainty_visibility_param (bool) : uncertainty visibility (default : True) (available in uncertainty defined)

        Args:
            column: (str) assay column name
        """
        super().__init__(column)

        if self.column.uncertainty.get_uncertainty_type() != UncertaintyType.UNDEFINED:
            self.uncertainty_visibility_param = ptree.Parameter.create(
                name=self.tr("Uncertainty"), type="bool", value=True, default=True
            )
            self.uncertainty_visibility_param.sigValueChanged.connect(
                self._uncertainty_visibility_changed
            )
        else:
            self.uncertainty_visibility_param = None

        self.symbol_group = ptree.Parameter.create(name=self.tr("Symbol"), type="group")
        self.symbol_group.setOpts(expanded=False)
        self.symbol_parameter = pTypes.ListParameter(
            name=self.tr("Symbol"),
            type="str",
            value="",
            default="",
            limits=["", "o", "s", "x", "d"],
        )
        self.symbol_group.addChild(self.symbol_parameter)
        self.symbol_size_parameter = pTypes.SimpleParameter(
            name=self.tr("Size"), type="int", value=5, default=5, min=1, max=20
        )
        self.symbol_group.addChild(self.symbol_size_parameter)
        self.symbol_color_parameter = pTypes.ColorParameter(
            name=self.tr("Color"), value="blue", default="blue"
        )
        self.symbol_group.addChild(self.symbol_color_parameter)

        self.color_ramp_group = ptree.Parameter.create(
            name=self.tr("Color ramp"), type="group"
        )
        self.color_ramp_group.setOpts(expanded=False)
        list_of_maps = pg.colormap.listMaps()
        # list_of_maps += pg.colormap.listMaps('matplotlib')
        # list_of_maps += pg.colormap.listMaps('colorcet')
        list_of_maps = sorted(list_of_maps, key=lambda x: x.swapcase())

        self.color_ramp_name_param = pTypes.ListParameter(
            name=self.tr("Color map name"),
            type="str",
            value="CET-L17",
            default="CET-L17",
            limits=list_of_maps,
        )
        self.color_ramp_group.addChild(self.color_ramp_name_param)

        self.color_ramp_parameter = pTypes.ColorMapParameter(name=self.tr("Color ramp"))
        self.color_ramp_group.addChild(self.color_ramp_parameter)
        self.min_color_ramp_param = pTypes.SimpleParameter(
            name=self.tr("Min"), type="float", value="0.0"
        )
        self.color_ramp_group.addChild(self.min_color_ramp_param)
        self.max_color_ramp_param = pTypes.SimpleParameter(
            name=self.tr("Max"), type="float", value="1.0"
        )
        self.color_ramp_group.addChild(self.max_color_ramp_param)
        self.pen_color_ramp_use_param = ptree.Parameter.create(
            name=self.tr("Use color ramp for pen"),
            type="bool",
            value=False,
            default=False,
        )
        self.color_ramp_group.addChild(self.pen_color_ramp_use_param)

        self.pen_color_ramp_use_param.sigValueChanged.connect(
            self._update_plot_item_color_ramp
        )
        self.color_ramp_name_param.sigValueChanged.connect(
            self._update_color_ramp_from_name
        )
        self.min_color_ramp_param.sigValueChanged.connect(
            self._update_plot_item_color_ramp
        )
        self.max_color_ramp_param.sigValueChanged.connect(
            self._update_plot_item_color_ramp
        )
        self.color_ramp_parameter.sigValueChanged.connect(
            self._update_plot_item_color_ramp
        )

        self._uncertainty_plot_items = []

    def _visibility_changed(self) -> None:
        """
        Update plot visibility when parameter is changed

        """
        super()._visibility_changed()
        for plot_item in self._uncertainty_plot_items:
            plot_item.setVisible(self.visibility_param.value())

    def _pen_updated(self) -> None:
        """
        Update plot pen when parameter is changed : use color ramp or pen value

        """
        if self.plot_item:
            self._update_plot_item_color_ramp()

    def stack_widget(self, plot_stacked: pg.PlotWidget) -> None:
        """
        Add current plot item to a new plot widget and remove them from current plot widget

        Args:
            plot_stacked (pg.PlotWidget): plot widget where item are added
        """
        super().stack_widget(plot_stacked)
        if self.plot_widget:
            for item in self._uncertainty_plot_items:
                self.plot_widget.removeItem(item)
                plot_stacked.addItem(item)

    def unstack_widget(self, plot_stacked: pg.PlotWidget) -> None:
        """
        Remove current plot item from a stacked plot widget and add them to current plot widget

        Args:
            plot_stacked (pg.PlotWidget): plot widget where items are available
        """
        super().unstack_widget(plot_stacked)
        if self.plot_widget:
            for item in self._uncertainty_plot_items:
                plot_stacked.removeItem(item)
                self.plot_widget.addItem(item)

    def set_uncertainty_plot_items(self, plot_items: [pg.PlotDataItem]) -> None:
        """
        Define plot items containing current assay data uncertainty

        Args:
            plot_items: [pg.PlotDataItem]
        """
        self._uncertainty_plot_items = plot_items

    def _uncertainty_visibility_changed(self) -> None:
        """
        Update plot uncertainty item visibility when parameter is changed

        """
        for plot_item in self._uncertainty_plot_items:
            plot_item.setVisible(self.uncertainty_visibility_param.value())

    def add_children_to_root_param(self, params: ptree.Parameter):
        super().add_children_to_root_param(params)
        if self.uncertainty_visibility_param:
            params.addChild(self.uncertainty_visibility_param)
        params.addChild(self.symbol_group)
        params.addChild(self.color_ramp_group)

    def set_plot_item(self, plot_item: pg.PlotDataItem) -> None:
        """
        Define plot item containing current assay data

        Args:
            plot_item: pg.PlotDataItem
        """
        super().set_plot_item(plot_item)
        if self.plot_item:
            # Define current parameters
            if self.symbol_parameter.value():
                self.plot_item.setSymbol(self.symbol_parameter.value())
            else:
                self.plot_item.setSymbol(None)
            self.plot_item.setSymbolSize(self.symbol_size_parameter.value())
            self.plot_item.setSymbolBrush(self.symbol_color_parameter.value())

            if self.plot_item.xData is not None and self.plot_item.yData is not None:
                if self.plot_item.domain == AssayDomainType.DEPTH:
                    min_ = min(self.plot_item.xData)
                    max_ = max(self.plot_item.xData)
                else:
                    min_ = min(self.plot_item.yData)
                    max_ = max(self.plot_item.yData)
                self.min_color_ramp_param.setValue(
                    min_, self._update_plot_item_color_ramp
                )
                self.min_color_ramp_param.setDefault(min_)
                self.max_color_ramp_param.setValue(
                    max_, self._update_plot_item_color_ramp
                )
                self.max_color_ramp_param.setDefault(max_)

            self.symbol_parameter.sigValueChanged.connect(
                lambda params, changes: self.plot_item.setSymbol(changes)
                if changes
                else self.plot_item.setSymbol(None)
            )
            self.symbol_size_parameter.sigValueChanged.connect(
                lambda params, changes: self.plot_item.setSymbolSize(changes)
            )
            self.symbol_color_parameter.sigValueChanged.connect(
                lambda params, changes: self.plot_item.setSymbolBrush(changes)
            )
            self._update_plot_item_color_ramp()

    def _update_color_ramp_from_name(self) -> None:
        """
        Update displayed color ramp from chosen name

        """
        cm = pg.colormap.get(self.color_ramp_name_param.value())
        self.color_ramp_parameter.setValue(cm)

    def _update_plot_item_color_ramp(self) -> None:
        """
        Update plot item color ramp from current parameter

        """
        if self.plot_item:
            cm = self.color_ramp_parameter.value()
            pen = self.pen_params.pen
            if cm and self.pen_color_ramp_use_param.value():
                pen = cm.getPen(
                    span=(
                        self.min_color_ramp_param.value(),
                        self.max_color_ramp_param.value(),
                    ),
                    width=pen.width(),
                    orientation="horizontal"
                    if self.plot_item.domain == AssayDomainType.DEPTH
                    else "vertical",
                )
                self.plot_item.setPen(pen)
            else:
                self.plot_item.setPen(pen)

        # brush = cm.getBrush(span=(min_, max_), orientation='vertical')
        # self.plot_item.setFillLevel(min_)
        # self.plot_item.setBrush(brush)

    def copy_from_config(self, other) -> None:
        """
        Copy configuration from another configuration.
        If a plot item is associated it will be updated

        Args:
            other: configuration to be copy
        """
        super().copy_from_config(other)
        if other.uncertainty_visibility_param and self.uncertainty_visibility_param:
            self.uncertainty_visibility_param.setValue(
                other.uncertainty_visibility_param.value()
            )

        self.symbol_parameter.setValue(other.symbol_parameter.value())
        self.symbol_size_parameter.setValue(other.symbol_size_parameter.value())
        self.symbol_color_parameter.setValue(other.symbol_color_parameter.value())

        self.color_ramp_name_param.setValue(other.color_ramp_name_param.value())
        self.color_ramp_parameter.setValue(other.color_ramp_parameter.value())
        self.min_color_ramp_param.setValue(
            other.min_color_ramp_param.value(), self._update_plot_item_color_ramp
        )
        self.max_color_ramp_param.setValue(
            other.max_color_ramp_param.value(), self._update_plot_item_color_ramp
        )
        self.pen_color_ramp_use_param.setValue(other.pen_color_ramp_use_param.value())
        self._update_plot_item_color_ramp()
