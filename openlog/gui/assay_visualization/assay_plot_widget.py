import pyqtgraph as pg
from qgis.PyQt import QtCore

from openlog.datamodel.assay.generic_assay import AssayDomainType
from openlog.gui.assay_visualization.assay_inspector_line import AssayInspectorLine


class AssayPlotWidget(pg.PlotWidget):
    def __init__(
        self,
        domain: AssayDomainType,
        parent=None,
        background="default",
        plotItem=None,
        **kargs
    ):
        """
        pg.PlotWidget override to store domain and extra functions to define limits and display AssayInspectorLine

        Args:
            domain: AssayDomainType
            parent:
            background:
            plotItem:
            **kargs:
        """
        super().__init__(parent, background, plotItem, **kargs)
        self.domain = domain
        self.setBackground("white")

        # Define axis from domain
        if self.domain == AssayDomainType.TIME:
            axis = pg.DateAxisItem()
            self.setAxisItems({"bottom": axis})
            self.getViewBox().setMouseEnabled(y=False)
        else:
            self.showAxis("top")

            self.getViewBox().invertY(True)  # Y inversion for depth assay
            self.getViewBox().setMouseEnabled(x=False)

        # Add an inspector line (not attached by default)
        self.inspector = AssayInspectorLine(domain=self.domain)
        self._sync_inspector = None
        self._connection_to_sync_inspector = None
        self._connection_from_sync_inspector = None

    def set_limits(self, min_domain_axis: float, max_domain_axis: float) -> None:
        """
        Define limit depending on current domain

        Args:
            min_domain_axis: float
            max_domain_axis: float
        """
        if self.domain == AssayDomainType.TIME:
            self.getViewBox().setLimits(xMin=min_domain_axis, xMax=max_domain_axis)
        else:
            self.getViewBox().setLimits(yMin=min_domain_axis, yMax=max_domain_axis)

        if not self.inspector.valueDefined:
            self.inspector.setPos(
                min_domain_axis + (max_domain_axis - min_domain_axis) / 2.0
            )
            self.inspector.valueDefined = True

    def enable_inspector_line(self, enable: bool) -> None:
        """
        Enable or disable inspector line for this plot

        Args:
            enable: (bool)
        """
        if enable:
            self.inspector.attachToPlotItem(self.getPlotItem())
        else:
            self.inspector.dettach()

    def enable_inspector_sync(
        self, sync_inspector: AssayInspectorLine, enable: bool
    ) -> None:
        """
        Enable inspector line synchronisation

        Args:
            sync_inspector: (AssayInspectorLine) inspector line to be synchronized
            enable: (bool) enable or disable sync
        """
        # Disable current sync
        if self._sync_inspector and self._connection_to_sync_inspector:
            self._sync_inspector.disconnect(self._connection_to_sync_inspector)
            self._sync_inspector = None
            self._connection_to_sync_inspector = None

        if self._connection_from_sync_inspector:
            self.inspector.sigPositionChanged.disconnect(
                self._connection_from_sync_inspector
            )
            self._connection_from_sync_inspector = None

        # Enable if asked
        if enable and sync_inspector != self.inspector:
            self._sync_inspector = sync_inspector
            self._connection_to_sync_inspector = (
                self._sync_inspector.sigPositionChanged.connect(
                    lambda inspector: self.inspector.setPos(inspector.getPos())
                )
            )
            self._connection_from_sync_inspector = (
                self.inspector.sigPositionChanged.connect(
                    lambda inspector: self._sync_inspector.setPos(inspector.getPos())
                )
            )

    def minimumSizeHint(self):
        """
        Return minimum size hint with width calculation from displayed title and axes

        Returns: (QSize) minimum size hint

        """
        res = super().minimumSizeHint()
        if self.plotItem.titleLabel:
            size_title_label = self.plotItem.titleLabel.sizeHint(
                QtCore.Qt.SizeHint.MinimumSize, 0
            )
            res.setWidth(int(size_title_label.width()))
        if self.getAxis("left"):
            size_axis = self.getAxis("left").minimumSize()
            res.setWidth(res.width() + int(size_axis.width()))
            if self.getAxis("left").label:
                res.setHeight(
                    int(self.getAxis("left").label.boundingRect().width() * 0.8)
                )  ## bounding rect is usually an overestimate
        if self.getAxis("right"):
            size_axis = self.getAxis("right").minimumSize()
            res.setWidth(res.width() + int(size_axis.width()))
        if self.getAxis("top"):
            size_axis = self.getAxis("top").minimumSize()
            res.setHeight(res.height() + int(size_axis.height()))
        if self.getAxis("bottom"):
            size_axis = self.getAxis("bottom").minimumSize()
            res.setHeight(res.height() + int(size_axis.height()))
        return res
