import json
import secrets
from dataclasses import dataclass
from pathlib import Path

from openlog.__about__ import DIR_PLUGIN_ROOT


@dataclass
class BarSymbology:
    """Class for bar symbology.

    Args:
        pattern (str): svg file for pattern
        scale (int): scale for svg
        color (str): html color as string
    """

    pattern: str
    scale: int
    color: str

    @classmethod
    def from_json(cls, data):
        """
        Define BarSymbology from json data

        Args:
            data: json data

        Returns: BarSymbologyMap

        """
        return cls(**data)


class BarSymbologyMap:
    # Alias that can be used to define svg pattern file path
    PLUGIN_ROOT_ALIAS = "{plugin_root}"
    USGS_SYMB_ROOT_ALIAS = "{usgs_symbology}"

    # Value for aliases
    USGS_SYMB_ROOT = "{plugin_root}/resources/usgs_symbology"

    def __init__(self):
        """
        Map to store symbology for a category : key category (str)

        """
        self.category_symbology_map = {}

    @classmethod
    def from_json(cls, data):
        """
        Define BarSymbologyMap from json data (see to_json)

        Args:
            data: json data

        Returns: BarSymbologyMap

        """
        res = cls()
        for k, v in data.items():
            res.category_symbology_map[k] = BarSymbology.from_json(v)
        return res

    def to_json(self) -> str:
        """
        Write BarSymbologyMap as JSON data

        Returns: JSON data

        """
        return json.dumps(
            self.category_symbology_map,
            default=lambda o: o.__dict__,
            sort_keys=True,
            indent=4,
        )

    def merge(self, other) -> None:
        """
        Merge map from a BarSymbologyMap

        Args:
            other: (BarSymbologyMap)
        """
        for key, config in other.category_symbology_map.items():
            self.category_symbology_map[key] = config

    def has_category(self, category: str) -> bool:
        """
        Check if a category is available in map

        Args:
            category: (str) category

        Returns: True if category available, False otherwise

        """
        return category in self.category_symbology_map.keys()

    def add_category(self, category: str, symbology: BarSymbology = None) -> None:
        """
        Add a category symbology to map (if no symbology defined get_random_symbology used)

        Args:
            category: (str) category
            symbology: (BarSymbology) symbology (default : None)
        """
        if symbology is None:
            self.category_symbology_map[category] = self.get_random_symbology()
        else:
            self.category_symbology_map[category] = symbology

    def get_random_symbology(self) -> BarSymbology:
        """
        Return a random symbology (color and usgs pattern)

        Returns: (BarSymbology) random symbology

        """

        def _get_random_pattern() -> str:
            usgs_code = secrets.choice(range(601, 733))
            return self.USGS_SYMB_ROOT_ALIAS + f"/usgs{usgs_code}.svg"

        # Random HTML color (6 values)
        color = "#" + "".join([secrets.choice("0123456789ABCDEF") for _ in range(6)])

        # Random usgs pattern (check if available)
        pattern_file = _get_random_pattern()
        while not Path(self.replace_alias_in_pattern(pattern_file)).exists():
            pattern_file = _get_random_pattern()

        return BarSymbology(
            pattern=pattern_file,
            scale=200,
            color=color,
        )

    def get_symbology(self, category: str) -> BarSymbology:
        """
        Get symbology for category. (If no symbology defined get_random_symbology used)

        Args:
            category: (str) category

        Returns: (BarSymbology) symbology

        """
        if self.has_category(category):
            symb = self.category_symbology_map[category]
        else:
            symb = self.get_random_symbology()
        return symb

    def replace_alias_in_pattern(self, pattern: str) -> str:
        """
        Replace alias in pattern string to get full path

        Args:
            pattern: (str) pattern with alias

        Returns: patern in aliases replaced

        """
        pattern = pattern.replace(self.USGS_SYMB_ROOT_ALIAS, self.USGS_SYMB_ROOT)
        pattern = pattern.replace(self.PLUGIN_ROOT_ALIAS, str(DIR_PLUGIN_ROOT))
        return pattern

    def add_alias_into_pattern(self, pattern: str) -> str:
        """
        Add alias into pattern to reduce pattern size

        Args:
            pattern: pattern with full path

        Returns: pattern with aliases

        """
        pattern = pattern.replace(str(DIR_PLUGIN_ROOT), self.PLUGIN_ROOT_ALIAS)
        pattern = pattern.replace(self.USGS_SYMB_ROOT, self.USGS_SYMB_ROOT_ALIAS)
        return pattern

    def get_pattern_file(self, category: str) -> str:
        """
        Get pattern file with alias conversion

        Args:
            category: (str) category

        Returns: (str) pattern file

        """
        svg_file = self.get_symbology(category).pattern
        svg_file = self.replace_alias_in_pattern(svg_file)
        return svg_file

    def get_scale(self, category: str) -> int:
        """
        Get scale

        Args:
            category: (str) category

        Returns: (int) scale

        """
        return self.get_symbology(category).scale

    def get_color(self, category: str) -> str:
        """
        Get color

        Args:
            category: (str) category

        Returns: (str) html color as string

        """
        return self.get_symbology(category).color
