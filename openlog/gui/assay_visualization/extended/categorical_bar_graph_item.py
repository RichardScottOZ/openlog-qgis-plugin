import numpy as np
from pyqtgraph import mkPen

from openlog.datamodel.assay.generic_assay import GenericAssay
from openlog.gui.assay_visualization.extended.bar_symbology import BarSymbologyMap
from openlog.gui.pyqtgraph.SvgBarGraphItem import SvgBarGraphItem


class CategoricalBarGraphItem(SvgBarGraphItem):

    WIDTH = 10

    def __init__(
        self, assay: GenericAssay, column: str, symbology_map: BarSymbologyMap
    ):
        """
        Override SvgBarGraphItem to display assay data as categorical series

        Args:
            assay: GenericAssay
            column: assay column name (should have an extended data extent and categorical series type)
            symbology_map: BarSymbologyMap for category display
        """
        self._assay = assay
        self._column = column
        self.symbology_map = symbology_map

        # For now display all values available
        (x, y) = assay.get_all_values(column, remove_none=True)
        x = x.astype(float)

        x0_val = np.full(len(x), 0)
        if assay.get_dimension(x.shape) == 2:
            y0_val = x[:, 0]
            height_val = [interval[1] - interval[0] for interval in x]
        else:
            y0_val = []
            height_val = []

        for category in y:
            if not self.symbology_map.has_category(str(category)):
                self.symbology_map.add_category(str(category))

        width_val = np.full(len(x), self.WIDTH)

        super().__init__(height=height_val, width=width_val, x0=x0_val, y0=y0_val)
        self.set_symbology_map(self.symbology_map)

    def set_symbology_map(self, symbology_map: BarSymbologyMap):
        """
        Define used bar symbology map

        Args:
            symbology_map: (BarSymbologyMap)
        """
        self.symbology_map = symbology_map

        # For now display all values available
        (x, y) = self._assay.get_all_values(self._column)

        self.svg_files = [symbology_map.get_pattern_file(str(cat)) for cat in y]
        self.svg_sizes = [symbology_map.get_scale(str(cat)) for cat in y]
        self.opts["brushes"] = [symbology_map.get_color(str(cat)) for cat in y]
        self.update_brush_texture()

    def setPen(self, *args, **kargs):
        """
        Sets the pen used to draw graph line.
        The argument can be a :class:`QtGui.QPen` or any combination of arguments accepted by
        :func:`pyqtgraph.mkPen() <pyqtgraph.mkPen>`.
        """
        pen = mkPen(*args, **kargs)
        self.opts["pen"] = pen
        self.picture = None
