# User manual

## Database creation

OpenLog provides its own borehole data storage library called [xplordb](https://gitlab.com/geolandia/openlog/xplordb).  
`xplordb` meets [GeoSciML](https://www.ogc.org/standards/geosciml) standards and relies on `PostgreSQL/Postgis` or `spatialite` for its backend component.

<div style="padding:52.92% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/733919902?h=ec4d0bcd08&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="How to set up a database in OpenLog"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

### Spatialite - standalone

Follow the instructions in the [quick start](./quick_start.md/#database-creation) database creation section.

### PostgreSQL - xplordb

To create a new `xplordb` database and provided that you have access to an active PostgreSQL + Postgis server:

   1. go to `OpenLog` menu > `Database` > `Create new xplordb database`

      ```{image} images/create_new_xplordb_01_menu.png
      :alt: create_new_xplordb_01_menu
      :class: bg-primary
      :width: 400px
      :align: center
      ```

   2. input host and port details to connect to the PostgreSQL backend database

      ```{image} images/create_new_xplordb_02_connection.png
      :alt: create_new_xplordb_02_connection
      :class: bg-primary
      :width: 400px
      :align: center
      ```

      > **_NOTE:_**  database creation is only available to PostgreSQL users with sufficient privileges

   3. review authentication parameters in the `Basic` tab
   4. verify authentication parameters by using the `Test connection button` then click `Next` button.
   5. input a database name and set remaining parameters as desired

      ```{image} images/create_new_xplordb_03_xplordb_parameters.png
      :alt: create_new_xplordb_03_xplordb_parameters
      :class: bg-primary
      :width: 400px
      :align: center
      ```

      > **_NOTE:_** the `admin` user is mandatory and should be an `xdb_admin`

   6. click `Finish`
   7. connect to the newly created database

## Database connection

> **_NOTE: _** OpenLog integrates to multiple 3rd party databases but it is more efficient to work with native `xplordb` or `spatialite` databases.

### To spatialite

1. go to `OpenLog` menu > `Database` > `Connect to spatialite database`
2. browse to the database file

### To PostgreSQL

1. go to `OpenLog` menu > `Database` > `Connect to xplordb database`
2. input the host and port details of the PostgreSQL backend database
3. input the `xplordb` database name
4. go to the Basic tab and input the `xplordb` `admin` credentials

### To Geotic

1. go to `OpenLog` menu > `Database` > `Connect to geotic database`
2. input the host and port details of the MSSQL backend database
3. input the `geotic` database name
4. input the `geotic` user login and password

### To BD Geo

1. got to `OpenLog` menu > `Database` > `Connect to BD Geo database`
2. input the host and port details of the PostgreSQL backend database
3. input `BD Geo` database name
4. input the `BD Geo` user login and password

## Importing data

Once connected to an `xplordb` or `spatialite` database you may import 3 files csv data:

1. go to `OpenLog` menu > `Database` > `Import collar, survey, lithology csv`

   ```{image} images/import_data_01_menu.png
   :alt: import_data_01_menu
   :class: bg-primary
   :width: 400px
   :align: center
   ```

2. select or create the person doing the data import job

   ```{image} images/import_data_02_person.png
   :alt: import_data_02_person
   :class: bg-primary
   :width: 300px
   :align: center
   ```

3. select or create the dataset where the data will be imported.

   ```{image} images/import_data_03_dataset.png
   :alt: import_data_03_dataset
   :class: bg-primary
   :width: 300px
   :align: center
   ```

   Then you can click on the `Next` button.

4. select the **collar** csv file and parameterize the column definition

   ```{image} images/import_data_04_collar.png
   :alt: import_data_04_collar
   :class: bg-primary
   :width: 500px
   :align: center
   ```

   The `sample data` section represents the data as extracted from the file.
   The `imported data` section represents the data as imported into the database.

   > **_NOTE: _** It is crucial to appropriately match the input the database fields in the `column definition` section

5. select the **collar** csv file and parameterize the column definition

   ```{image} images/import_data_05_survey.png
   :alt: import_data_05_survey
   :class: bg-primary
   :width: 500px
   :align: center
   ```

   > **_NOTE: _** It is crucial to appropriately match the input the database fields in the `column definition` section

   Then you can click on the `Next` button.

6. select the **lithology** csv file and parameterize the column definition

   ```{image} images/import_data_06_litho.png
   :alt: import_data_06_litho
   :class: bg-primary
   :width: 500px
   :align: center
   ```

   > **_NOTE: _** It is crucial to appropriately match the input the database fields in the `column definition` section

7. review the summary of what **will be** imported into the database

   ```{image} images/import_data_07_summary.png
   :alt: import_data_07_summary
   :class: bg-primary
   :width: 200px
   :align: center
   ```

8. (optional) right-click the newly created `collar` layer and select `Zoom to layer` to display your data.

## Adding collar

```{warning}
TODO
```


## Desurveying

```{warning}
TODO
```
