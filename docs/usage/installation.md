# Installation

## Prerequisite

OpenLog requires the following general [dependencies](#direct-dependency-installations) to be met

* Python 3.9
* Python packages: `psycopg2`,  `pandas`, `sqlalchemy`, `GeoAlchemy2`, `xplordb`, `pyqtgraph`, `numpy`
* Python optional package : `pyodbc`

When running a xplordb database, the following additional dependencies shall be met

* PostgreSQl 12+
* PostGIS 3

## Versions

### Stable version

This plugin version is **not available for now**. It will be published on the official QGIS plugins repository: <https://plugins.qgis.org/plugins/openlog/>.

### Nightly build

```{warning}
The nightly build is not a production version and is aimed at alpha testers and developers only. Be careful, this version can be unstable.
```

To install OpenLog - QGIS plugin execute the following steps.

1. Add OpenLog - QGIS plugin repository:
   1. Go in the `Settings` tab of the QGIS `Plugins Manager` window (see [official documentation](https://docs.qgis.org/3.16/en/docs/user_manual/plugins/plugins.html#the-settings-tab))
   1. Add `https://geolandia.gitlab.io/openlog/openlog-qgis-plugin/plugins.xml` to the QGIS extensions repository list
   1. Hit the `Reload all Repositories` button.
   1. Enable `Show also experimental plugins`.

1. Activate the OpenLog - QGIS plugin:
   1. go to `Plugins` -> `Manage and Install Plugins` -> `All`
   1. search for "OpenLog"
   1. hit `Install Plugin`.

   <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/724419940?h=73e324cd21&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="How to install OpenLog 0.x.x alpha [Windows 10 - Linux]"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## Direct dependency installations

### Linux

```{Note}
Tested against Debian >= 10, Ubuntu >= 20.04, Mint ls >= 20.1
```

#### Python system [Linux]

To install Python 3.9 (strictly), run:

```bash
sudo apt-get install python3 python3-pip
```

#### Python mandatory package

OpenLog - QGIS Plugin depends on several python package :

| Package                | Website                                          | Description                                                                 |
|------------------------|--------------------------------------------------|-----------------------------------------------------------------------------|
| `pandas>=1.4,<=1.4.4"` | <https://pandas.pydata.org/>                     | Data analysis and manipulation tool                                         |
| `psycopg2`             | <https://www.psycopg.org/docs/>                  | PostgreSQL database adapter for Python                                      |
| `sqlalchemy>=1.4"`     | <https://www.sqlalchemy.org/>                    | Python SQL Toolkit and Object Relation Mapper                               |
| `GeoAlchemy2`          | <https://geoalchemy-2.readthedocs.io/en/latest/> | sqlalchemy extension for spatial database (Spatialite and Postgis)          |
| `xplordb>=0.13.0`      | <https://gitlab.com/geolandia/openlog/xplordb>   | Mineral Exploration Database template/ system for Postgres/PostGIS and QGIS |
| `pyqtgraph`            | <https://pyqtgraph.readthedocs.io/en/latest/>    | Data visualization library for Python using Qt GUI                          |
| `numpy`                | <https://numpy.org/>                             | Python package for scientific computation                                   |
To install the above packages, run:

```bash
pip install -U psycopg2 "pandas>=1.4,<=1.4.4" "sqlalchemy>=1.4" GeoAlchemy2 pyqtgraph numpy "xplordb>=0.13.0"
```

#### Python optional package

| Package  | Website                                 | Description                                                                                |
|----------|-----------------------------------------|--------------------------------------------------------------------------------------------|
| `pyodbc` | <https://github.com/mkleehammer/pyodbc> | Python module for ODBC database access, required by `sqlalchemy` for MSSQL database access |

To install the above packages, run:

```bash
pip install pyodbc
```

```{note}
You may need to install some external packages for compilation. See <https://github.com/mkleehammer/pyodbc/wiki/Install> for `pyodbc`.
```


### Windows 10

```{note}
Mainly tested on Windows 10.0.19044.
```

#### Python system [Windows]

The Windows version of QGIS is prepackaged with its own Python 3.9 install.

#### Embedded install

Dependencies are embedded in the released version of the plugin, in the `embedded_external_libs` folder.

On install, the plugin goes through the following steps:

1. attempt package import from the Python interpreter used by QGIS
2. on failure, add the `embedded_external_libs` subfolder to the `PYTHONPATH` and attempt package import from there
3. on failure, disable plugin and issue a warning message

```{warning}
Those packages require to be compiled with the same Python version than the one used by QGIS
```

#### Manual install

1. Review the [`requirements/embedded.txt`](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/blob/main/requirements/embedded.txt) for accurate package versions

2. Launch the `OSGeo4W Shell`, found either at:
   * `C:\Program Files\QGIS` (standalone), or
   * `C:\OSGeo4W` (OSGeo4W default).

3. Run (example with QGIS 3.22 LTR):

   ```batch
   python-qgis-ltr -m pip install -U pip
   python-qgis-ltr -m pip install -U setuptools wheel
   python-qgis-ltr -m pip install -U chevron sqlalchemy GeoAlchemy2 pyqtgraph xplordb>=0.13.0
   ```
