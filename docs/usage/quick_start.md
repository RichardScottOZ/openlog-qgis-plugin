# Quick start

OpenLog provides its own borehole data storage library called [xplordb](https://gitlab.com/geolandia/openlog/xplordb).  
`xplordb` meets [GeoSciML](https://www.ogc.org/standards/geosciml) standards and relies on `PostgreSQL/Postgis` or `spatialite` for its backend component.

## Database creation

<div style="padding:52.92% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/733919902?h=ec4d0bcd08&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="How to set up a database in OpenLog"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

### xplordb Spatialite

To create a new `spatialite` database:

1. go to `OpenLog` menu -> `Database` -> `Create new spatialite database`

    ```{image} images/create_new_xplordb_01_menu.png
    :alt: create_new_xplordb_01_menu
    :class: bg-primary
    :width: 400px
    :align: center
    ```

2. Choose where to save the database file.

You are now connected to the `spatialite` database and ready to import data.

### xplordb PostgreSQL/PostGIS

Follow the instructions in the [user manual](./user_manual.md).

## Importing 3 files csv data

To import 3 files csv data into your database:

1. go to `OpenLog` menu > `Database` > `Import collar, survey, lithology csv`

    ```{image} images/import_data_01_menu.png
    :alt: import_data_01_menu
    :class: bg-primary
    :width: 400px
    :align: center
    ```
  
2. select or create the person doing the data import job

3. select or create the dataset where the data will be imported

4. select the **collar** csv file and parameterize the column definition

5. select the **survey** csv file and parameterize the column definition

6. select the **lithology** csv file and parameterize the column definition

7. review the summary of what **will be** imported into the database

8. (optional) right-click the newly created `collar` layer and select `Zoom to layer` to display your data.

## Importing assay

To import assay into your database:

1. go to `OpenLog` menu > `Database` > `Import assay`

    ```{image} images/import_assay_01_menu.png
    :alt: import_assay_01_menu
    :class: bg-primary
    :width: 400px
    :align: center
    ```
  
2. select or create an assay 

3. check assay database definition or specify configuration in case of creation

4. select the **assay** csv file and parameterize the column definition

5. review the summary of what **will be** imported into the database

## Displaying assay

```{Note}
For now, only depth visualization is available. Time assay will be displayed as depth assay.
```

To visualize assay from your database:

1. go to `OpenLog` menu > Check 'Depth assay visualization'

    ```{image} images/depth_assay_visu_01_menu.png
    :alt: import_assay_01_menu
    :class: bg-primary
    :width: 400px
    :align: center
    ```
  
2. Depth assay visualization widget should be available 

3. add assay with 'Add' button

4. select collar from collar layer

Available features :
- define assays and collar visibility
- add / remove assay
- collar selection synchronization from collar layer

Upcoming features :
- plot item symbology (color, line type, symbol)
- new plot item types (histogram)
- time assay visualization
- stacked assays




