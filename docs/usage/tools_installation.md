# Tools and dependencies installation

## PostGreSQL and PostGIS

OpenLog - QGIS Plugin uses XplorDb python library in background. Xplordb depends on [PostgreSQL](https://www.postgresql.org), we recommend PostgreSQL version 14 and PostGIS version 3.

:::{warning}
make sure, if you have multiple versions of PostgreSQL installed at once on your machine, to use the right network port (one port per version).
:::

You will find installation instructions for your operating system on [the dedicated documentation page](https://www.postgresql.org/download/).

Once PostgreSQL is installed, install PostGIS by following [the installation instructions](https://postgis.net/install/) for your operating system.

:::{warning}
Regular users are expected to connect to a remote xplordb database with appropriate credentials (provided by database administrators).
:::

## Microsoft SQL Server driver

OpenLog - QGIS Plugin can connect to Geotic database. To enable Geotic database support, you must install the Microsoft SQL Server driver. 

Here are the installation instructions for [Linux](https://docs.microsoft.com/fr-fr/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver15#ubuntu) and for [Windows](https://go.microsoft.com/fwlink/?linkid=2186919).

:::{warning}
Regular users are expected to connect to a remote Geotic database with appropriate credentials (provided by database administrators).
:::
