# OpenLog for QGIS

Development of an Open Source drillhole visualization and editing module in QGIS Mining industry professionals are in natural need of solutions for drillhole visualization, management, and edition.

Although several free or Open Source solutions exist on the market to partially address the need, none has propagated into common use.

In partnership with a team of mining industry leaders, Oslandia intends to develop a high performance drillhole visualization QGIS module supporting 3D, cross-section, and log views.



```{toctree}
---
caption: Usage
maxdepth: 1
numbered: true
---
Installation <usage/installation>
Other tools installation <usage/tools_installation>
Quick start <usage/quick_start>
User Manual <usage/user_manual>
```

```{toctree}
---
caption: Contribution guide
maxdepth: 1
---
Code documentation <_apidoc/modules>
development/contribute
development/environment
development/documentation
development/translation
development/packaging
development/testing
development/history
```

----

> **Author and contributors:** {{ author }}  
> **Plugin version:** {{ version }}  
> **QGIS minimum version:** {{ qgis_version_min }}  
> **QGIS maximum version:** {{ qgis_version_max }}  
> **Source code:** {{ repo_url }}  
> **License:** {{ license }}  
> **Last documentation update:** {{ date_update }}  

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![flake8](https://img.shields.io/badge/linter-flake8-green)](https://flake8.pycqa.org/)
